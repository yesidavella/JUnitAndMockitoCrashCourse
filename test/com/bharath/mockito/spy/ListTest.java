package com.bharath.mockito.spy;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class ListTest {
	
	@Spy
	List<String> myList = new ArrayList<String>();
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void test() {
		
		myList.add("Yesid");
		myList.add("Pedro");
		
		Mockito.doReturn(5).when(myList).size();
		
		assertSame(5, myList.size());
	}

}
