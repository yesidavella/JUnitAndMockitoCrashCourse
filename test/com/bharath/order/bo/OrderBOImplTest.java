package com.bharath.order.bo;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.bharath.order.bo.exception.BOException;
import com.bharath.order.dao.OrderDAO;
import com.bharath.order.dto.Order;

public class OrderBOImplTest {

	private static final int ORDER_ID = 123;

	OrderBOImpl bo;
	
	@Mock
	OrderDAO dao;
	
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		bo = new OrderBOImpl();
		bo.setDao(dao);
	}
	
	@Test
	public void placeOrder_should_create_an_order() throws SQLException, BOException {
		
		Order order = new Order();
		when(dao.create(any(Order.class))).thenReturn(new Integer(1));
		
		boolean result = bo.placeOrder( any(Order.class) );
		
		assertTrue(result);
		verify(dao, atLeast(1)).create( any(Order.class) );	
	}
	
	@Test
	public void placeOrder_should_not_create_an_order() throws SQLException, BOException {
		
		Order order = new Order();
		when(dao.create(order)).thenReturn(new Integer(0));
		
		boolean result = bo.placeOrder(order);
		
		assertFalse(result);
		verify(dao).create(order);	
	}
	
	
	@Test(expected=BOException.class)
	public void placeOrder_should_throw_BOExeption() throws SQLException, BOException {
		
		Order order = new Order();
		when(dao.create(order)).thenThrow(SQLException.class);
		
		bo.placeOrder(order);	
	}
	
	@Ignore
	@Test
	public void cancelOrder_should_cancel_the_order() throws SQLException, BOException{
		
		Order order = new Order();
		when(dao.read(ORDER_ID)).thenReturn(order);
		when(dao.update(order)).thenReturn(new Integer(1));
		
		boolean result = bo.cancelOrder(ORDER_ID);
		
		assertTrue(result);
		verify(dao).read(ORDER_ID);
		verify(dao).update(order);
//		verify(dao).create(order); 
	}
	
	@Ignore
	@Test
	public void cancelOrder_should_not_cancel_the_order() throws SQLException, BOException{
		
		Order order = new Order();
		when(dao.read(ORDER_ID)).thenReturn(order);
		when(dao.update(order)).thenReturn(new Integer(0));
		
		boolean result = bo.cancelOrder(123);
		
		assertFalse(result);
		verify(dao).read(ORDER_ID);
		verify(dao).update(order); 
	}
	
	@Ignore
	@Test(expected=BOException.class)
	public void cancelOrder_should_throw_BOExeption_onRead() throws SQLException, BOException{

		when(dao.read(ORDER_ID)).thenThrow(SQLException.class);
		
		boolean result = bo.cancelOrder(ORDER_ID);
	}
	
	@Ignore
	@Test(expected=BOException.class)
	public void cancelOrder_should_throw_BOExeption_onUpdate() throws SQLException, BOException{
		
		Order order = new Order();
		
		when(dao.read(ORDER_ID)).thenReturn(order);
		when(dao.update(order)).thenThrow(SQLException.class);
		
		bo.cancelOrder(ORDER_ID);
		verify(dao).update(order); 
	}
	
	@Ignore
	@Test
	public void deleteOrder_should_delete_order() throws SQLException, BOException{
		
		when(dao.delete(ORDER_ID)).thenReturn(1);
		boolean result = bo.deleteOrder(ORDER_ID);		
		assertTrue(result);
		verify(dao).delete(ORDER_ID);
	}

}
