package com.yesid.mockito.scrapbook;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class ATest {
	
	@Mock
	B b;
	private A a;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		a = new A(b);
	}
	
	@Test
	public void usesVoidMethodShoulCallTheVoidMethod() throws Exception {
		doNothing().when(b).voidMethod();
		assertSame(1, a.usesVoidMethod());
		verify(b).voidMethod();
	}
	
	@Test(expected=RuntimeException.class)
	public void testConsecutiveCalls() throws Exception{
		doNothing().doThrow(Exception.class).when(b).voidMethod();
		a.usesVoidMethod();
		verify(b).voidMethod();
		a.usesVoidMethod();
		verify(b, times(1)).voidMethod();
	}

}
