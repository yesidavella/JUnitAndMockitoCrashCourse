Course Updated on December 7th , 2015

I added a new Bonus section "Section 11 - Spring Testing Quick Start " . Enjoy :)

Are you a java developer who want to become an expert at Unit Testing ? Then this course is a quick practical guide for you. Learn how to write real unit tests using JUnit and Mockito. This course will simplify things with concepts and step by step implementations .

There are so many java professionals who write great code , but not unit tests. This course aims at filling that gap by covering JUnit and Mockito the two required frameworks to write good unit tests.

Learn and master the most popular unit testing technologies in this comprehensive course.

Understand the importance of writing Unit Tests
Demystify the topics of Mocking
Write Unit tests using JUnit and Mockito
Learn what test coverage is and how to measure it
Run unit tests as a part of your Maven build
All in easy steps
Mastering Unit Testing for Java Professionals:

JUnit is a open source Framework to test and assert code behavior . Mockito mocks out the dependencies and stub the expectations allowing testing a particular layer/unit in isolation.

Any java developer at any level can access the key lessons and concepts in this course and learn to write quality unit tests.

This course is perfect for every java developer who works on building high quality applications .

Contents and Overview:

In over 2 hours of lectures this course covers necessary JUnit API and its usage with Mockito.

This course covers the importance of unit testing , how to effectively use JUnit ,how mocking works and how to use Mockito to write real unit tests.

Up on completion you will be able to test Java and JavaEE applications ,run unit tests as a part of your build and measure code coverage and improve it.

¿Cuáles son los requisitos?

Java Knowledge
Eclipse IDE Kepler(or Higher) For Java Developers
¿Qué voy a aprender en este curso?

Learn what Unit Testing is
Understand the importance of Unit Testing
Learn the JUnit Framework and API
Learn what mocking is and why we should mock
Write tests using JUnit and Mockito
Learn What Test Coverage is and how to measure it
All in simple and easy steps
Spring Testing Quick Start
PowerMock Quick Start
Parameterized JUnit
¿A quién está dirigido?

Anyone who wants to write quality code
Student with java background
Java Developers
Java EE Developers
***********************************************************************************************************
Command line instructions

Git global setup

git config --global user.name "Yesid"
git config --global user.email "yeavella@gmail.com"

Create a new repository

git clone https://gitlab.com/yesidavella/JUnitAndMockitoCrashCourse.git
cd JUnitAndMockitoCrashCourse
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder or Git repository

cd existing_folder
git init
git remote add origin https://gitlab.com/yesidavella/JUnitAndMockitoCrashCourse.git
git add .
git commit
git push -u origin master